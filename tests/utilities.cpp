// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "tests/utilities.hpp"

namespace utf8decoders {
namespace tests {

string
to_hex(const u8_char_t* bytes, size_t count) {
    string s(count*4, ' ');
    for (size_t i = 0; i < count; ++i) {
        s[4*i+0] = 'x';
        s[4*i+1] = hex_map[(bytes[i] & 0xF0) >> 4];
        s[4*i+2] = hex_map[bytes[i] & 0x0F];
    }
    return s;
}

} // namespace tests
} // namespace utf8decoders
