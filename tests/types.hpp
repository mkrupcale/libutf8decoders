// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_TESTS_TYPES_HPP
#define LIBUTF8DECODERS_TESTS_TYPES_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/api.hpp"
#ifdef U8D_HAVE_ICONV
#include "libutf8decoders/iconv/iconv.hpp"
#else
#error "iconv required for testing round-trip encode-decode"
#endif
#include "libutf8decoders/types.hpp"
#include "libutf8decoders/unicode.hpp"

namespace utf8decoders {
namespace tests {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u8_char_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u8_s_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using InputIt = const u8_char_t*;
#endif

using ::utf8decoders::DECODE_NEXT_STATUS;
using ::utf8decoders::DECODE_STATUS;

using ::utf8decoders::decode_next_f;
using ::utf8decoders::decode_next_impl;
using ::utf8decoders::decode_next_impls;
using ::utf8decoders::decode_f;
using ::utf8decoders::decode_impl;
using ::utf8decoders::decode_impls;

using ::utf8decoders::iconv::iconv;

using ::utf8decoders::unicode::SURROGATE_BASE;
using ::utf8decoders::unicode::BMP_PRIVATE_BASE;
using ::utf8decoders::unicode::BMP_RESERVED_NONCHARACTER_BASE;
using ::utf8decoders::unicode::BMP_RESERVED_NONCHARACTER_END;
using ::utf8decoders::unicode::REPLACEMENT_CHARACTER;
using ::utf8decoders::unicode::SPUA_B_END;

} // namespace tests
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_TESTS_TYPES_HPP */
