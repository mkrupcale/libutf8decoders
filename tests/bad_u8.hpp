// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_TESTS_BAD_U8_HPP
#define LIBUTF8DECODERS_TESTS_BAD_U8_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/utilities.hpp"

namespace utf8decoders {
namespace tests {

namespace u8d_util = ::utf8decoders::utilities;

// See Unicode standard 12.0 Chapter 3, D92
// Table 3-7. Well-Formed UTF-8 Byte Sequences
#ifdef U8D_USE_STD_STRING_VIEW
#define cs_to_u8(x) u8d_util::to_u8_sv(x)
static const u8_sv_t bad_u8_sequences[] =
#else
#define cs_to_u8(x) u8d_util::to_u8_s(x)
static const u8_s_t bad_u8_sequences[] =
#endif
{
    {cs_to_u8("\x80")},
    {cs_to_u8("\xC1\xBF")},
    {cs_to_u8("\xC2\x7F")},
    {cs_to_u8("\xC2\xC0")},
    {cs_to_u8("\xE0\x9F\x80")},
    {cs_to_u8("\xE0\x9F\xBF")},
    {cs_to_u8("\xE0\xA0\x7F")},
    {cs_to_u8("\xE0\xBF\xC0")},
    {cs_to_u8("\xE1\x7F\x80")},
    {cs_to_u8("\xE1\x7F\xBF")},
    {cs_to_u8("\xEC\x80\x7F")},
    {cs_to_u8("\xEC\xBF\xC0")},
    {cs_to_u8("\xED\x7F\x80")},
    {cs_to_u8("\xED\x7F\xBF")},
    {cs_to_u8("\xED\x80\x7F")},
    {cs_to_u8("\xED\x9F\xC0")},
    {cs_to_u8("\xEE\x7F\x80")},
    {cs_to_u8("\xEE\x7F\xBF")},
    {cs_to_u8("\xEF\x80\x7F")},
    {cs_to_u8("\xEF\xBF\xC0")},
    {cs_to_u8("\xF0\x8F\x80\x80")},
    {cs_to_u8("\xF0\x8F\x80\xBF")},
    {cs_to_u8("\xF0\x8F\xBF\x80")},
    {cs_to_u8("\xF0\x8F\xBF\xBF")},
    {cs_to_u8("\xF0\x90\x7F\x80")},
    {cs_to_u8("\xF0\x90\xC0\xBF")},
    {cs_to_u8("\xF0\x90\x80\x7F")},
    {cs_to_u8("\xF0\x90\xBF\xC0")},
    {cs_to_u8("\xF0\xBF\x7F\x80")},
    {cs_to_u8("\xF0\xBF\x80\x7F")},
    {cs_to_u8("\xF0\xBF\xBF\xC0")},
    {cs_to_u8("\xF0\xBF\xC0\xBF")},
    {cs_to_u8("\xF0\xC0\x80\x80")},
    {cs_to_u8("\xF0\xC0\x80\xBF")},
    {cs_to_u8("\xF0\xC0\xBF\x80")},
    {cs_to_u8("\xF0\xC0\xBF\xBF")},
    {cs_to_u8("\xF1\x7F\x80\x80")},
    {cs_to_u8("\xF1\x7F\x80\xBF")},
    {cs_to_u8("\xF1\x7F\xBF\x80")},
    {cs_to_u8("\xF1\x7F\xBF\xBF")},
    {cs_to_u8("\xF1\x80\x7F\x80")},
    {cs_to_u8("\xF1\x80\xC0\xBF")},
    {cs_to_u8("\xF1\x80\x80\x7F")},
    {cs_to_u8("\xF1\x80\xBF\xC0")},
    {cs_to_u8("\xF3\xBF\x7F\x80")},
    {cs_to_u8("\xF3\xBF\x80\x7F")},
    {cs_to_u8("\xF3\xBF\xBF\xC0")},
    {cs_to_u8("\xF3\xBF\xC0\xBF")},
    {cs_to_u8("\xF3\xC0\x80\x80")},
    {cs_to_u8("\xF3\xC0\x80\xBF")},
    {cs_to_u8("\xF3\xC0\xBF\x80")},
    {cs_to_u8("\xF3\xC0\xBF\xBF")},
    {cs_to_u8("\xF1\x7F\x80\x80")},
    {cs_to_u8("\xF1\x7F\x80\xBF")},
    {cs_to_u8("\xF1\xC0\x80\x80")},
    {cs_to_u8("\xF1\xC0\x80\xBF")},
    {cs_to_u8("\xF3\x7F\xBF\x80")},
    {cs_to_u8("\xF3\x7F\xBF\xBF")},
    {cs_to_u8("\xF3\xC0\xBF\x80")},
    {cs_to_u8("\xF3\xC0\xBF\xBF")},
    {cs_to_u8("\xF4\x7F\x80\x80")},
    {cs_to_u8("\xF4\x7F\x80\xBF")},
    {cs_to_u8("\xF4\x7F\xBF\x80")},
    {cs_to_u8("\xF4\x7F\xBF\xBF")},
    {cs_to_u8("\xF4\x80\x7F\x80")},
    {cs_to_u8("\xF4\x80\xC0\xBF")},
    {cs_to_u8("\xF4\x80\x80\x7F")},
    {cs_to_u8("\xF4\x80\xBF\xC0")},
    {cs_to_u8("\xF4\x8F\x7F\x80")},
    {cs_to_u8("\xF4\x8F\x80\x7F")},
    {cs_to_u8("\xF4\x8F\xBF\xC0")},
    {cs_to_u8("\xF4\x8F\xC0\xBF")},
    {cs_to_u8("\xF4\x90\x80\x80")},
    {cs_to_u8("\xF4\x90\x80\xBF")},
    {cs_to_u8("\xF4\x90\xBF\x80")},
    {cs_to_u8("\xF4\x90\xBF\xBF")},
    {cs_to_u8("\xF5\x80\x80\x80")},
    {cs_to_u8("\xF8\x80\x80\x80\xAF")},
    {cs_to_u8("\xF8\x87\xBF\xBF\xBF")},
    {cs_to_u8("\xFC\x80\x80\x80\x80\xAF")},
    {cs_to_u8("\xFC\x83\xBF\xBF\xBF\xBF")}
};

#undef cs_to_u8

} // namespace tests
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_TESTS_BAD_U8_HPP */
