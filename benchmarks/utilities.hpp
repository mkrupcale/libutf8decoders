// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_BENCHMARKS_UTILITIES_HPP
#define LIBUTF8DECODERS_BENCHMARKS_UTILITIES_HPP

#include "benchmarks/types.hpp"

namespace utf8decoders {
namespace benchmarks {

u8_s_t
read_file(const char* filename);

} // namespace benchmarks
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_BENCHMARKS_UTILITIES_HPP */
