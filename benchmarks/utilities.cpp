// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include <ios>
#include <fstream>

#include "benchmarks/types.hpp"

namespace utf8decoders {
namespace benchmarks {

using ::std::ios;
using ::std::streamsize;
using ::std::ifstream;

u8_s_t
read_file(const char* filename) {
    ifstream in(filename, ios::in | ios::binary);
    in.exceptions(ifstream::failbit | ifstream::badbit | ifstream::eofbit);
    u8_s_t data;
    in.seekg(0, ios::end);
    data.resize(static_cast<u8_s_t::size_type>(in.tellg()));
    in.seekg(0, ios::beg);
    in.read(reinterpret_cast<char*>(&*data.begin()), static_cast<streamsize>(data.size()));
    in.close();
    return data;
}

} // namespace benchmarks
} // namespace utf8decoders
