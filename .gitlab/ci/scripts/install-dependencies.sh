#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

LIBUTF8DECODERS_COMPONENTS=(boost_text
                            icu
                            llvm
                            mk
                            python
                            utf8proc)

DEFAULT_CC=gcc
DEFAULT_CXX=g++

function print_usage ()
{
    echo "Usage: "
    echo "${BASH_SOURCE} [--help] <options> targets..."
    echo ""
    echo "Options"
    echo "  -h,--help"
    echo "    Prints help"
    echo "  -b,--build"
    echo "    Installs build dependencies"
    echo ""
    echo "Targets"
    echo "  all"
    echo "    Install all target dependencies"
    echo "  libutf8decoders"
    echo "    Install all libutf8decoders dependencies"
    echo "  tests"
    echo "    Install all tests dependencies"
    echo "  benchmarks"
    echo "    Install all benchmarks dependencies"
}

# get_build_pkgs cc cxx buildsys
function get_build_pkgs ()
{
    local cc="$1"
    local cxx="$2"
    local buildsys="$3"
    pkgs+=("${cc}")
    if [ "${cxx}" == g++ ]; then
        pkgs+=("gcc-c++")
    elif [ "${cxx}" == clang++ ]; then
        pkgs+=("clang")
    fi
    if [ "${buildsys}" == cmake ]; then
        pkgs+=("cmake" "ninja-build")
    fi
}

# get_boost_text_pkgs [build]
function get_boost_text_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("boost-devel")
    fi
}

# get_icu_pkgs [build]
function get_icu_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("libicu-devel")
    else
        pkgs+=("libicu")
    fi
}

# get_llvm_pkgs [build]
function get_llvm_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("llvm-devel")
    else
        pkgs+=("llvm-libs")
    fi
}

# get_mk_pkgs [build]
function get_mk_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("libicu-devel")
    fi
}

# get_python_pkgs [build]
function get_python_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("python3-devel")
    else
        pkgs+=("python3-libs")
    fi
}

# get_utf8proc_pkgs [build]
function get_utf8proc_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("utf8proc-devel")
    else
        pkgs+=("utf8proc")
    fi
}

# get_tests_pkgs [build]
function get_tests_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("catch-devel")
    fi
}

# get_benchmarks_pkgs [build]
function get_benchmarks_pkgs ()
{
    local build="$1"
    if [ -n "${build}" ]; then
        pkgs+=("google-benchmark-devel")
    else
        pkgs+=("google-benchmark")
    fi
}

# install_pkgs pkgs
function install_pkgs ()
{
    local pkgs=("$@")
    if [ "${#pkgs[@]}" -ne 0 ]; then
        rpm -q "${pkgs[@]}" || dnf -y install "${pkgs[@]}"
    fi
}

while [ $# -gt 0 ]; do
    case "$1" in
        -h|--help)
            print_usage
            exit 0
            ;;
        -b|--build)
            INSTALL_BUILD_DEPS=true
            ;;
        "libutf8decoders")
            INSTALL_LIBUTF8DECODERS_DEPS=true
            TARGET_SPECIFIED=true
            ;;
        "tests")
            INSTALL_LIBUTF8DECODERS_DEPS=true
            INSTALL_TESTS_DEPS=true
            TARGET_SPECIFIED=true
            ;;
        "benchmarks")
            INSTALL_LIBUTF8DECODERS_DEPS=true
            INSTALL_BENCHMARKS_DEPS=true
            TARGET_SPECIFIED=true
            ;;
    esac
    shift
done

if [ -z "${TARGET_SPECIFIED}" ]; then
    INSTALL_LIBUTF8DECODERS_DEPS=true
    INSTALL_TESTS_DEPS=true
    INSTALL_BENCHMARKS_DEPS=true
fi

pkgs=()

cc="${cc:-${DEFAULT_CC}}"
cxx="${cxx:-${DEFAULT_CXX}}"

if [ "${INSTALL_BUILD_DEPS}" == true ]; then
    get_build_pkgs "${cc}" "${cxx}" cmake
fi

if [ "${INSTALL_LIBUTF8DECODERS_DEPS}" == true ]; then
    for component in "${LIBUTF8DECODERS_COMPONENTS[@]}"; do
        get_"${component}"_pkgs "${INSTALL_BUILD_DEPS}"
    done
fi

if [ "${INSTALL_TESTS_DEPS}" == true ]; then
    get_tests_pkgs "${INSTALL_BUILD_DEPS}"
fi

if [ "${INSTALL_BENCHMARKS_DEPS}" == true ]; then
    get_benchmarks_pkgs "${INSTALL_BUILD_DEPS}"
fi

install_pkgs "${pkgs[@]}"
