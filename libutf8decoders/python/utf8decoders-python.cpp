// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/python/utf8decoders-python.hpp"

#include <memory>

#include "Python.h"

#include "libutf8decoders/api.hpp"

namespace utf8decoders {
namespace python {

using ::utf8decoders::DECODE_STATUS;

auto py_obj_deleter = [](PyObject* py_obj) {
                          Py_DECREF(py_obj);
                      };

using py_obj_t = std::unique_ptr<PyObject, decltype(py_obj_deleter)>;

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const char*;
    using OutputChar = ::Py_UCS4;
    using OutputIt = OutputChar*;
    using ssize_t = ptrdiff_t;

    InputIt src_begin = reinterpret_cast<InputIt>(&*src.cbegin());
    ssize_t src_size = static_cast<ssize_t>(src.size());

    OutputIt dst_it = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;

    py_obj_t py_unicode{::PyUnicode_DecodeUTF8(src_begin, src_size, "replace"),
                        py_obj_deleter};

    if (PyUnicode_READY(py_unicode.get()) == -1)
        return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);

    ssize_t num_cp = PyUnicode_GET_LENGTH(py_unicode.get());
    int kind = PyUnicode_KIND(py_unicode.get());
    void* data = PyUnicode_DATA(py_unicode.get());

    for (ssize_t i = 0; i < num_cp; ++i) {
        *dst_it++ = PyUnicode_READ(kind, data, i);
    }

    return num_cp;
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const char*;
    using OutputChar = ::Py_UCS4;
    using OutputIt = OutputChar*;
    using ssize_t = ptrdiff_t;

    InputIt src_begin = reinterpret_cast<InputIt>(src);
    ssize_t src_size = static_cast<ssize_t>(count);

    OutputIt dst_it = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;

    py_obj_t py_unicode{::PyUnicode_DecodeUTF8(src_begin, src_size, "replace"),
                        py_obj_deleter};

    if (PyUnicode_READY(py_unicode.get()) == -1)
        return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);

    ssize_t num_cp = PyUnicode_GET_LENGTH(py_unicode.get());
    int kind = PyUnicode_KIND(py_unicode.get());
    void* data = PyUnicode_DATA(py_unicode.get());

    for (ssize_t i = 0; i < num_cp; ++i) {
        *dst_it++ = PyUnicode_READ(kind, data, i);
    }

    return num_cp;
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace python
} // namespace utf8decoders
