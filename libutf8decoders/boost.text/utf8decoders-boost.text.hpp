// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_BOOST_TEXT_UTF8DECODERS_BOOST_TEXT_HPP
#define LIBUTF8DECODERS_BOOST_TEXT_UTF8DECODERS_BOOST_TEXT_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "boost/text/utf8.hpp"

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace boost_text {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

using ::utf8decoders::DECODE_NEXT_STATUS;

#ifdef U8D_USE_STD_STRING_VIEW

U8D_CONSTEXPR ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = u8_sv_t::const_pointer;

    InputIt src_begin = &*src.cbegin();
    InputIt src_end = &*src.cend();

    auto utf32_it = ::boost::text::utf8::to_utf32_iterator<InputIt>(src_begin, src_begin, src_end);
    code_point = *utf32_it;
    return code_point != ::boost::text::utf8::replacement_character() || src_begin == src_end ?
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK) :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept;

#else /* U8D_USE_STD_STRING_VIEW */

U8D_CONSTEXPR ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;

    InputIt src_begin = src;
    InputIt src_end = src + count;

    auto utf32_it = ::boost::text::utf8::to_utf32_iterator<InputIt>(src_begin, src_begin, src_end);
    code_point = *utf32_it;
    return code_point != ::boost::text::utf8::replacement_character() || src_begin == src_end ?
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK) :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept;

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace boost_text
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_BOOST_TEXT_UTF8DECODERS_BOOST_TEXT_HPP */
