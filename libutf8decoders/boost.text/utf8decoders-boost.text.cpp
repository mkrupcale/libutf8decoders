// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/boost.text/utf8decoders-boost.text.hpp"

#include <algorithm>

namespace utf8decoders {
namespace boost_text {

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = u8_sv_t::const_pointer;
    using OutputIt = u32_s_t::pointer;

    InputIt src_begin = &*src.cbegin();
    InputIt src_end = &*src.cend();
    OutputIt dst_begin = &*dst.begin() + dst_offset;

    auto begin_utf32 = ::boost::text::utf8::to_utf32_iterator<InputIt>(src_begin, src_begin, src_end);
    auto end_utf32 = ::boost::text::utf8::to_utf32_iterator<InputIt>(src_begin, src_end, src_end);

    return ::std::copy(begin_utf32, end_utf32, dst_begin) - dst_begin;
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const u8_char_t*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_begin = src;
    InputIt src_end = src_begin + count;
    OutputIt dst_begin = &*dst.begin() + dst_offset;

    auto begin_utf32 = ::boost::text::utf8::to_utf32_iterator<InputIt>(src_begin, src_begin, src_end);
    auto end_utf32 = ::boost::text::utf8::to_utf32_iterator<InputIt>(src_begin, src_end, src_end);

    return ::std::copy(begin_utf32, end_utf32, dst_begin) - dst_begin;
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace boost_text
} // namespace utf8decoders
