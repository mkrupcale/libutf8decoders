// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/av/utf8decoders-av.hpp"

#include <cstddef>
#include <cstdint>

#include "av_utf8.h"

#include "libutf8decoders/api.hpp"

namespace utf8decoders {
namespace av {

using ::utf8decoders::DECODE_STATUS;

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const char*;
    using OutputIt = ::std::int32_t*;
    using size_t = ::std::size_t;

    InputIt src_begin = reinterpret_cast<InputIt>(&*src.cbegin());
    size_t src_size = src.size();
    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    size_t dst_size = dst.size();

    ptrdiff_t cp = ::utf8_to_wchar(src_begin, src_size, dst_begin, dst_size, 0);

    return cp != -1 ? cp : static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const char*;
    using OutputIt = ::std::int32_t*;
    using size_t = ::std::size_t;

    InputIt src_begin = reinterpret_cast<InputIt>(src);
    size_t src_size = count;
    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    size_t dst_size = dst.size();

    ptrdiff_t cp = ::utf8_to_wchar(src_begin, src_size, dst_begin, dst_size, 0);

    return cp != -1 ? static_cast<ptrdiff_t>(cp) : static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace av
} // namespace utf8decoders
