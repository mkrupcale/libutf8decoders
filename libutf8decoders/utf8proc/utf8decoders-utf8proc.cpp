// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/utf8proc/utf8decoders-utf8proc.hpp"

#include "libutf8decoders/api.hpp"

namespace utf8decoders {
namespace utf8proc {

using ::utf8decoders::DECODE_NEXT_STATUS;

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;
    using OutputIt = ::std::int32_t*;
    using ssize_t = ptrdiff_t;

    InputIt src_begin = &*src.cbegin();
    ssize_t src_size = static_cast<ssize_t>(src.size());
    OutputIt dst_begin = reinterpret_cast<OutputIt>(&code_point);

    ptrdiff_t cu{::utf8proc_iterate(src_begin, src_size, dst_begin)};

    return cu >= 0 ? cu : static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;
    using OutputIt = ::std::int32_t*;
    using ssize_t = ptrdiff_t;

    InputIt src_begin = src;
    ssize_t src_size = static_cast<ssize_t>(count);
    OutputIt dst_begin = reinterpret_cast<OutputIt>(&code_point);

    ptrdiff_t cu{::utf8proc_iterate(src_begin, src_size, dst_begin)};

    return cu >= 0 ? cu : static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace utf8proc
} // namespace utf8decoders
