// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_UTF8DECODERS_HPP
#define LIBUTF8DECODERS_UTF8DECODERS_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/api.hpp"

#ifdef U8D_HAVE_AV
#include "libutf8decoders/av/utf8decoders-av.hpp"
#endif
#ifdef U8D_HAVE_BH
#include "libutf8decoders/bh/utf8decoders-bh.hpp"
#endif
#ifdef U8D_HAVE_BOOST_TEXT
#include "libutf8decoders/boost.text/utf8decoders-boost.text.hpp"
#endif
#ifdef U8D_HAVE_BRANCHLESS
#include "libutf8decoders/branchless/utf8decoders-branchless.hpp"
#endif
#ifdef U8D_HAVE_CODECVT
#include "libutf8decoders/codecvt/utf8decoders-codecvt.hpp"
#endif
#ifdef U8D_HAVE_ICONV
#include "libutf8decoders/iconv/utf8decoders-iconv.hpp"
#endif
#ifdef U8D_HAVE_ICU
#include "libutf8decoders/icu/utf8decoders-icu.hpp"
#include "libutf8decoders/icu/icu.hpp"
#include "unicode/ustring.h" // u_strFromUTF8
#endif
#ifdef U8D_HAVE_KEWB
#include "libutf8decoders/kewb/utf8decoders-kewb.hpp"
#endif
#ifdef U8D_HAVE_LLVM
#include "libutf8decoders/llvm/utf8decoders-llvm.hpp"
#endif
#ifdef U8D_HAVE_MK
#include "libutf8decoders/mk/utf8decoders-mk.hpp"
#include "libutf8decoders/mk/icu.hpp"
#include "libutf8decoders/mk/kewb.hpp"
#endif
#ifdef U8D_HAVE_PYTHON
#include "libutf8decoders/python/utf8decoders-python.hpp"
#endif
#ifdef U8D_HAVE_UTF8PROC
#include "libutf8decoders/utf8proc/utf8decoders-utf8proc.hpp"
#endif

#include "libutf8decoders/types.hpp"

namespace utf8decoders {

struct decode_next_impl {
    const char* name;
    decode_next_f f;
};

struct decode_impl {
    const char* name;
    decode_f f;
};

#define IMPL_ENTRY(f) {#f, f},

static U8D_CONSTEXPR_DECL const decode_next_impl decode_next_impls[] = {
#ifdef U8D_HAVE_BH
    IMPL_ENTRY(bh::decode_next<bh::decode_next_stateful_branchless_0>)
    IMPL_ENTRY(bh::decode_next<bh::decode_next_stateful_branchless_1>)
    IMPL_ENTRY(bh::decode_next<bh::decode_next_stateful_branching_0>)
#endif
#ifdef U8D_HAVE_BOOST_TEXT
    IMPL_ENTRY(boost_text::decode_next)
#endif
#ifdef U8D_HAVE_BRANCHLESS
    IMPL_ENTRY(branchless::decode_next)
#endif
#ifdef U8D_HAVE_CODECVT
    IMPL_ENTRY(codecvt::decode_next)
#endif
#ifdef U8D_HAVE_ICONV
    IMPL_ENTRY(iconv::decode_next)
#endif
#ifdef U8D_HAVE_ICU
    IMPL_ENTRY(icu::u8_next)
    IMPL_ENTRY(icu::u8_next_unsafe)
#endif
#ifdef U8D_HAVE_KEWB
    IMPL_ENTRY(kewb::decode_next<::uu::UtfUtils::AdvanceWithSmallTable>)
    IMPL_ENTRY(kewb::decode_next<::uu::UtfUtils::AdvanceWithBigTable>)
#endif
#ifdef U8D_HAVE_LLVM
    IMPL_ENTRY(llvm::decode_next)
#endif
#ifdef U8D_HAVE_MK
    IMPL_ENTRY(mk::decode_next<mk::icu::decode_next>)
    IMPL_ENTRY(mk::decode_next<mk::kewb::decode_next>)
#endif
#ifdef U8D_HAVE_UTF8PROC
    IMPL_ENTRY(utf8proc::decode_next)
#endif
};

static U8D_CONSTEXPR_DECL const decode_impl decode_impls[] = {
#ifdef U8D_HAVE_AV
    IMPL_ENTRY(av::decode)
#endif
#ifdef U8D_HAVE_BH
    IMPL_ENTRY(bh::decode<bh::decode_next_stateful_branchless_0>)
    IMPL_ENTRY(bh::decode<bh::decode_next_stateful_branchless_1>)
    IMPL_ENTRY(bh::decode<bh::decode_next_stateful_branching_0>)
#endif
#ifdef U8D_HAVE_BOOST_TEXT
    IMPL_ENTRY(boost_text::decode)
#endif
#ifdef U8D_HAVE_BRANCHLESS
    IMPL_ENTRY(branchless::decode)
#endif
#ifdef U8D_HAVE_CODECVT
    IMPL_ENTRY(codecvt::decode)
#endif
#ifdef U8D_HAVE_ICONV
    IMPL_ENTRY(iconv::decode)
#endif
#ifdef U8D_HAVE_ICU
    IMPL_ENTRY(decode<icu::u8_next>)
    IMPL_ENTRY(decode<icu::u8_next_unsafe>)
    IMPL_ENTRY(icu::decode<::u_strFromUTF8>)
    IMPL_ENTRY(icu::decode<::u_strFromUTF8Lenient>)
#endif
#ifdef U8D_HAVE_KEWB
    IMPL_ENTRY(kewb::decode<::uu::UtfUtils::BasicSmallTableConvert>)
    IMPL_ENTRY(kewb::decode<::uu::UtfUtils::FastSmallTableConvert>)
    IMPL_ENTRY(kewb::decode<::uu::UtfUtils::SseSmallTableConvert>)
    IMPL_ENTRY(kewb::decode<::uu::UtfUtils::BasicBigTableConvert>)
    IMPL_ENTRY(kewb::decode<::uu::UtfUtils::FastBigTableConvert>)
    IMPL_ENTRY(kewb::decode<::uu::UtfUtils::SseBigTableConvert>)
#endif
#ifdef U8D_HAVE_LLVM
    IMPL_ENTRY(llvm::decode)
#endif
#ifdef U8D_HAVE_MK
    IMPL_ENTRY(decode<mk::decode_next<mk::icu::decode_next>>)
    IMPL_ENTRY(decode<mk::decode_next<mk::kewb::decode_next>>)
#endif
#ifdef U8D_HAVE_PYTHON
    IMPL_ENTRY(python::decode)
#endif
#ifdef U8D_HAVE_UTF8PROC
    IMPL_ENTRY(decode<utf8proc::decode_next>)
#endif
};

#undef IMPL_ENTRY

} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_UTF8DECODERS_HPP */
