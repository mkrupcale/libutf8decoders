// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/codecvt/utf8decoders-codecvt.hpp"

#include <locale>

#include "libutf8decoders/api.hpp"

namespace utf8decoders {
namespace codecvt {

using ::utf8decoders::DECODE_NEXT_STATUS;
using ::utf8decoders::DECODE_STATUS;

using InternT = char32_t;
using ExternT = char;

static const ::std::locale loc = ::std::locale("C.utf8");
static const auto& facet = ::std::use_facet<::std::codecvt<InternT, ExternT, ::std::mbstate_t>>(loc);

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode_next(u8_sv_t src, char32_t& code_point) noexcept {
    using InternT = char32_t;
    using ExternT = char;
    using InputIt = const ExternT*;
    using OutputIt = InternT*;

    InputIt src_begin = reinterpret_cast<InputIt>(&*src.cbegin());
    InputIt src_end = reinterpret_cast<InputIt>(&*src.cend());
    InputIt src_it{};
    OutputIt dst_begin = &code_point;
    OutputIt dst_end = dst_begin + 1;
    OutputIt dst_it{};

    ::std::mbstate_t mb = ::std::mbstate_t();

    ::std::codecvt_base::result r{facet.in(mb,
                                           src_begin, src_end, src_it,
                                           dst_begin, dst_end, dst_it)};
    return r == ::std::codecvt_base::result::ok ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InternT = char32_t;
    using ExternT = char;
    using InputIt = const ExternT*;
    using OutputIt = InternT*;

    InputIt src_begin = reinterpret_cast<InputIt>(&*src.cbegin());
    InputIt src_end = reinterpret_cast<InputIt>(&*src.cend());
    InputIt src_it{};
    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_end = &*dst.end();
    OutputIt dst_it{};

    ::std::mbstate_t mb = ::std::mbstate_t();

    ::std::codecvt_base::result r{facet.in(mb,
                                           src_begin, src_end, src_it,
                                           dst_begin, dst_end, dst_it)};
    return r == ::std::codecvt_base::result::ok ?
        dst_it - dst_begin :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InternT = char32_t;
    using ExternT = char;
    using InputIt = const ExternT*;
    using OutputIt = InternT*;

    InputIt src_begin = reinterpret_cast<InputIt>(src);
    InputIt src_end = src_begin + count;
    InputIt src_it{};
    OutputIt dst_begin = &code_point;
    OutputIt dst_end = dst_begin + 1;
    OutputIt dst_it{};

    ::std::mbstate_t mb = ::std::mbstate_t();

    ::std::codecvt_base::result r{facet.in(mb,
                                           src_begin, src_end, src_it,
                                           dst_begin, dst_end, dst_it)};
    return r == ::std::codecvt_base::result::ok ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset) noexcept {
    using InternT = char32_t;
    using ExternT = char;
    using InputIt = const ExternT*;
    using OutputIt = InternT*;

    InputIt src_begin = reinterpret_cast<InputIt>(src);
    InputIt src_end = src_begin + count;
    InputIt src_it{};
    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_end = &*dst.end();
    OutputIt dst_it{};

    ::std::mbstate_t mb = ::std::mbstate_t();

    ::std::codecvt_base::result r{facet.in(mb,
                                           src_begin, src_end, src_it,
                                           dst_begin, dst_end, dst_it)};
    return r == ::std::codecvt_base::result::ok ?
        dst_it - dst_begin :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace codecvt
} // namespace utf8decoders
