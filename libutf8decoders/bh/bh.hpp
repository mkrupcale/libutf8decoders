// SPDX-License-Identifier: MIT
// Copyright (c) 2008-2009 Bjoern Hoehrmann <bjoern@hoehrmann.de>
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>
// See http://bjoern.hoehrmann.de/utf-8/decoder/dfa/ for details.

#ifndef LIBUTF8DECODERS_BH_BH_HPP
#define LIBUTF8DECODERS_BH_BH_HPP

#include "libutf8decoders/features.hpp"

#include <cstdint>

namespace utf8decoders {
namespace bh {

static U8D_CONSTEXPR_DECL const ::std::uint8_t UTF8_ACCEPT = 0;
static U8D_CONSTEXPR_DECL const ::std::uint8_t UTF8_REJECT = 12;

static U8D_CONSTEXPR_DECL const ::std::uint8_t utf8d[] = {
    // The first part of the table maps bytes to character classes that
    // to reduce the size of the transition table and create bitmasks.
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    10,3,3,3,3,3,3,3,3,3,3,3,3,4,3,3, 11,6,6,6,5,8,8,8,8,8,8,8,8,8,8,8,

    // The second part is a transition table that maps a combination
    // of a state of the automaton and a character class to a state.
    0,12,24,36,60,96,84,12,12,12,48,72, 12,12,12,12,12,12,12,12,12,12,12,12,
    12, 0,12,12,12,12,12, 0,12, 0,12,12, 12,24,12,12,12,12,12,24,12,24,12,12,
    12,12,12,12,12,12,12,24,12,12,12,12, 12,24,12,12,12,12,12,12,12,24,12,12,
    12,12,12,12,12,12,12,36,12,36,12,12, 12,36,12,12,12,12,12,36,12,36,12,12,
    12,36,12,12,12,12,12,12,12,12,12,12,
};

U8D_CONSTEXPR ::std::uint8_t
decode_next_stateful_branchless_0(::std::uint8_t byte,
                                  u32_char_t& code_point,
                                  ::std::uint8_t& state) noexcept {
    ::std::uint8_t type = utf8d[byte];

    code_point = (state != UTF8_ACCEPT) ?
        (byte & 0x3F) | (code_point << 6) :
        (0xFFu >> type) & byte;

    state = utf8d[256 + state + type];
    return state;
}

U8D_CONSTEXPR ::std::uint8_t
decode_next_stateful_branchless_1(::std::uint8_t byte,
                                  u32_char_t& code_point,
                                  ::std::uint8_t& state) noexcept {
    ::std::uint8_t type = utf8d[byte];

    code_point = (state > UTF8_REJECT) ?
        (byte & 0x3Fu) | (code_point << 6) :
        (0xFFu >> type) & byte;

    state = utf8d[256 + state + type];
    return state;
}

U8D_CONSTEXPR ::std::uint8_t
decode_next_stateful_branching_0(::std::uint8_t byte,
                                 u32_char_t& code_point,
                                 ::std::uint8_t& state) noexcept {
    ::std::uint8_t type = utf8d[byte];

    if (state != UTF8_ACCEPT) {
        state = utf8d[256 + state + type];
        code_point = (code_point << 6) | (byte & 63);
    } else {
        state = utf8d[256 + state + type];
        code_point = byte & (255u >> type);
    }
    return state;
}

} // namespace bh
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_BH_BH_HPP */
