// SPDX-License-Identifier: Unicode-DFS-2016
// Copyright 2016 and later: Unicode, Inc. and others.
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_MK_ICU_HPP
#define LIBUTF8DECODERS_MK_ICU_HPP

#include "libutf8decoders/features.hpp"

#include <cstdint>

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace mk {
namespace icu {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u8_char_t;

using ::utf8decoders::DECODE_NEXT_STATUS;

static U8D_CONSTEXPR_DECL const u8_char_t LEAD3_T1_BITS[] = {
    0x20, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x10, 0x30, 0x30
};

static U8D_CONSTEXPR_DECL const u8_char_t LEAD4_T1_BITS[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x0F, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x00
};

U8D_CONSTEXPR bool
u8_is_single(u8_char_t c) noexcept {
    return (c & 0x80) == 0;
}

U8D_CONSTEXPR bool
u8_is_valid_lead3_and_t1(u32_char_t& lead, u8_char_t t1, u8_char_t& cu) noexcept {
    return LEAD3_T1_BITS[lead &= 0xF] & (1 << ((cu = t1) >> 5));
}

U8D_CONSTEXPR bool
u8_is_valid_lead4_and_t1(u32_char_t& lead, u8_char_t t1, u8_char_t& cu) noexcept {
    return LEAD4_T1_BITS[(cu = t1) >> 4] & (1 << (lead & 0x7));
}

U8D_CONSTEXPR bool
u8_offset_in_range(u8_char_t c, u8_char_t lower, u8_char_t range, u8_char_t& offset) noexcept {
    return (offset = static_cast<u8_char_t>(c - lower)) <= range;
}

U8D_CONSTEXPR u32_char_t
u8_add_code_unit(u32_char_t code_point, u8_char_t cu) noexcept {
    return (code_point << 6) | cu;
}

U8D_CONSTEXPR ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;

    InputIt src_begin = src;
    InputIt src_it = src_begin;
    InputIt src_end = src + count;

    if (src_begin == src_end)
        return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK);

    code_point = static_cast<u32_char_t>(*src_it++);
    if (!u8_is_single(static_cast<u8_char_t>(code_point))) {
        u8_char_t cu = 0;
        if (src_it != src_end &&
            // fetch/validate/assemble all but last trail byte
            (code_point >= 0xE0 ?
             (code_point < 0xF0 ?
              // U+0800..U+FFFF except surrogates
              u8_is_valid_lead3_and_t1(code_point, *src_it, cu) &&
              (cu &= 0x3F, 1) :
              // U+10000..U+10FFFF
              (code_point -= 0xF0) <= 4 &&
              u8_is_valid_lead4_and_t1(code_point, *src_it, cu) &&
              (code_point = u8_add_code_unit(code_point, cu & 0x3F), ++src_it != src_end) &&
              u8_offset_in_range(*src_it, 0x80, 0x3F, cu)) &&
             // valid second-to-last trail byte
             (code_point = u8_add_code_unit(code_point, cu), ++src_it != src_end) :
             // U+0080..U+07FF
             code_point >= 0xC2 && (code_point &= 0x1F, 1)) &&
            // last trail byte
            u8_offset_in_range(*src_it, 0x80, 0x3F, cu) &&
            (code_point = u8_add_code_unit(code_point, cu), ++src_it, 1)) {
        } else {
            // invalid sequence
            return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
        }
    }
    return src_it - src_begin;
}

} // namespace icu
} // namespace mk
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_MK_ICU_HPP */
