// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_ICU_UTF8DECODERS_ICU_HPP
#define LIBUTF8DECODERS_ICU_UTF8DECODERS_ICU_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include <cstdint>

#include "unicode/umachine.h" // UChar
#include "unicode/utypes.h" // E_FAILURE

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"
#include "libutf8decoders/utf16.hpp"

namespace utf8decoders {
namespace icu {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

using ::utf8decoders::decode_next_f;
using ::utf8decoders::DECODE_STATUS;

using decode_f = ::UChar* (*)(::UChar* dest,
                              ::std::int32_t dest_capacity,
                              ::std::int32_t* p_dest_length,
                              const char* src,
                              ::std::int32_t src_length,
                              ::UErrorCode* p_error_code);

static U8D_CONSTEXPR_DECL const ::std::uint8_t bytes_per_UChar = sizeof(::UChar);
static U8D_CONSTEXPR_DECL const ::std::uint8_t bytes_per_code_point = sizeof(u32_char_t);
static U8D_CONSTEXPR_DECL const ::std::uint8_t UChar_per_code_point =
    bytes_per_code_point / bytes_per_UChar;

#ifdef U8D_USE_STD_STRING_VIEW

template<decode_f d>
ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = const char*;
    using OutputIt = ::UChar*;
    using ssize_t = ::std::int32_t;

    InputIt src_begin = reinterpret_cast<InputIt>(&*src.cbegin());
    ssize_t src_size = static_cast<ssize_t>(src.size());

    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    ssize_t dst_capacity = static_cast<ssize_t>(dst.size() - dst_offset) * UChar_per_code_point;
    ssize_t dst_size;

    ::UErrorCode e{};

    d(dst_begin, dst_capacity, &dst_size, src_begin, src_size, &e);

    if (U_FAILURE(e))
        return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);

    return ::utf8decoders::utf16::decode(
        {dst_begin, static_cast<u8_sv_t::size_type>(dst_size)}, dst, dst_offset);
}

#else /* U8D_USE_STD_STRING_VIEW */

template<decode_f d>
ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = const char*;
    using OutputIt = ::UChar*;
    using ssize_t = ::std::int32_t;

    InputIt src_begin = reinterpret_cast<InputIt>(src);
    ssize_t src_size = static_cast<ssize_t>(count);

    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    ssize_t dst_capacity = static_cast<ssize_t>(dst.size() - dst_offset) * UChar_per_code_point;
    ssize_t dst_size;

    ::UErrorCode e{};

    d(dst_begin, dst_capacity, &dst_size, src_begin, src_size, &e);

    if (U_FAILURE(e))
        return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);

    return ::utf8decoders::utf16::decode(dst_begin, static_cast<size_t>(dst_size), dst, dst_offset);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace icu
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_ICU_UTF8DECODERS_ICU_HPP */
