// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_ICU_ICU_HPP
#define LIBUTF8DECODERS_ICU_ICU_HPP

#include "libutf8decoders/features.hpp"

#include <cstdint>

#include "unicode/utf8.h" // U8_NEXT_UNSAFE, U8_INTERNAL_NEXT_OR_SUB

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace icu {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

using ::utf8decoders::DECODE_NEXT_STATUS;

U8D_CONSTEXPR void
u8_next_unsafe(const ::std::uint8_t* src, ::std::int32_t& src_offset,
               u32_char_t& code_point) noexcept {
    U8_NEXT_UNSAFE(src, src_offset, code_point);
}

U8D_CONSTEXPR void
u8_next(const ::std::uint8_t* src, ::std::int32_t& src_offset,
        ::std::int32_t src_size, u32_char_t& code_point) noexcept {
    U8_INTERNAL_NEXT_OR_SUB(src, src_offset, src_size, code_point,
                            static_cast<u32_char_t>(DECODE_NEXT_STATUS::ERROR));
}

#ifdef U8D_USE_STD_STRING_VIEW

U8D_CONSTEXPR ptrdiff_t
u8_next_unsafe(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = const ::std::uint8_t*;
    using ssize_t = ::std::int32_t;

    InputIt src_begin = &*src.cbegin();
    ssize_t src_offset = 0;

    if (src.size() == 0)
        return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK);

    u8_next_unsafe(src_begin, src_offset, code_point);
    return src_offset;
}

U8D_CONSTEXPR ptrdiff_t
u8_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = const ::std::uint8_t*;
    using ssize_t = ::std::int32_t;

    InputIt src_begin = &*src.cbegin();
    ssize_t src_offset = 0;
    ssize_t src_size = static_cast<ssize_t>(src.size());

    if (src_size == 0)
        return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK);

    u8_next(src_begin, src_offset, src_size, code_point);
    return code_point != static_cast<u32_char_t>(DECODE_NEXT_STATUS::ERROR) ?
        src_offset :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

U8D_CONSTEXPR ptrdiff_t
u8_next_unsafe(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const ::std::uint8_t*;
    using ssize_t = ::std::int32_t;

    InputIt src_begin = src;
    ssize_t src_offset = 0;

    if (count == 0)
        return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK);

    u8_next_unsafe(src_begin, src_offset, code_point);
    return src_offset;
}

U8D_CONSTEXPR ptrdiff_t
u8_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const ::std::uint8_t*;
    using ssize_t = ::std::int32_t;

    InputIt src_begin = src;
    ssize_t src_offset = 0;
    ssize_t src_size = static_cast<ssize_t>(count);

    if (src_size == 0)
        return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK);

    u8_next(src_begin, src_offset, src_size, code_point);
    return code_point != static_cast<u32_char_t>(DECODE_NEXT_STATUS::ERROR) ?
        src_offset :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace icu
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_ICU_ICU_HPP */
