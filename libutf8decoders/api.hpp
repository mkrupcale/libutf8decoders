// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_API_HPP
#define LIBUTF8DECODERS_API_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/types.hpp"

namespace utf8decoders {

enum class DECODE_NEXT_STATUS : ptrdiff_t {
    ERROR = -1,
    OK = 0
};

enum class DECODE_STATUS : ptrdiff_t {
    ERROR = -1,
    OK = 0
};

#ifdef U8D_USE_STD_STRING_VIEW

template<decode_next_f dn>
U8D_CONSTEXPR ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    return dn(src, code_point);
}

template<decode_next_f dn>
ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = u8_sv_t::const_pointer;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = &*src.cbegin();
    InputIt src_end = &*src.cend();
    size_t src_units = 0;

    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    u32_char_t code_point = 0;

    while (src_it < src_end) {
        if ((src_units =
             static_cast<size_t>(
                 dn({src_it, static_cast<size_t>(src_end - src_it)}, code_point))) !=
            static_cast<size_t>(DECODE_NEXT_STATUS::ERROR)) {
            src_it += src_units;
            *dst_it++ = code_point;
        } else {
            return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
        }
    }
    return dst_it - dst_begin;
}

template<decode_f d>
U8D_CONSTEXPR ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0)
    noexcept(noexcept(d(src, dst, dst_offset))) {
    return d(src, dst, dst_offset);
}

#else /* U8D_USE_STD_STRING_VIEW */

template<decode_next_f dn>
U8D_CONSTEXPR ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    return dn(src, count, code_point);
}

template<decode_next_f dn>
ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = const u8_char_t*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = src;
    InputIt src_end = src_it + count;
    size_t src_units = 0;

    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    u32_char_t code_point = 0;

    while (src_it < src_end) {
        if ((src_units = static_cast<size_t>(
                 dn(src_it, static_cast<size_t>(src_end - src_it), code_point))) !=
            static_cast<size_t>(DECODE_NEXT_STATUS::ERROR)) {
            src_it += src_units;
            *dst_it++ = code_point;
        } else {
            return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
        }
    }
    return dst_it - dst_begin;
}

template<decode_f d>
U8D_CONSTEXPR ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0)
    noexcept(noexcept(d(src, count, dst, dst_offset))) {
    return d(src, count, dst, dst_offset);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_API_HPP */
