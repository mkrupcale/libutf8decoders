# libutf8decoders

## Introduction

`libutf8decoders` is a library of different UTF-8[1] decoder implementations which offers a common API for their usage. The primary aim of this project is to test and benchmark the different approaches.

The current list of decoders supported includes:

 - Alexey Vatchenko (AV)
 - Bjoern Hoehrmann (BH)[2]
 - Boost.Text[3,4]
 - branchless[5]
 - `std::codecvt`[6]
 - `iconv(3)`[7]
 - ICU[8-10]
 - KEWB[11,12]
 - LLVM[13]
 - Matthew Krupcale (MK)
 - Python[14]
 - utf8proc[15]

Most of these decoders use a relatively straight-forward approach, incrementally testing for the number of code units in the code point and decoding as it goes. The bh and KEWB implementations, however, use a deterministic finite automaton (DFA) state machine to decode the code unit sequence, and the branchless implementation uses some small lookup tables to decode without branching.

## Installation

This library currently uses the CMake build system. To install, clone this repo, and run `cmake`:

```shell
git clone https://gitlab.com/mkrupcale/libutf8decoders.git
cd libutf8decoders
git submodule update --init # retrieve benchmark data
mkdir build
cd build
cmake ..
make -j$(nproc)
```

### Requirements

Some of the decoder implementations have external dependencies:

 - Boost.Text
     - boost
 - ICU
     - libicu
     - libicuuc
 - LLVM
     - libLLVM (optional: will use bundled Support library component if not found)
 - MK
     - libicu
 - Python
     - libpython
 - utf8proc
     - libutf8proc

Bundled with `libutf8decoders` are some additional external dependencies which are not commonly packaged. These can be found in the `external` top-level directory, and they can additionally be retrieved using `scripts/fetch-external.sh`, which requires `wget`.

The tests and benchmarks require additionally Catch2 and Google benchmark, respectively, while the benchmarks also require data in the `libutf8decoders-bench-data`[16] repository.

## Usage

The library comes with a CMake configuration file, so you can use it in CMake with `find_package`:

```cmake
find_package(utf8decoders REQUIRED CONFIG)
target_link_libraries(example utf8decoders::utf8decoders)
```

### API

The basic API provided by each decoder implementation is contained in `libutf8decoders/api.hpp`. There are two variants of the API: one with a `string_view` interface and one with a traditional C-like pointer plus size interface. Which interface is provided is controlled by the `U8D_USE_STRING_VIEW` CMake option. By default it is `ON` and will try to find `std::string_view` and use it if found. This will fail for pre-C++17 standard libraries but can be disabled even for C++17 and later. Aside from the function parameters, there are two basic functions provided in the API: `decode_next` and `decode`. They have the following basic function prototype (defined in `libutf8decoders/types.hpp`):

```c++
#ifdef U8D_USE_STD_STRING_VIEW

using decode_next_f = ptrdiff_t (*)(u8_sv_t src,
                                    u32_char_t& code_point) U8D_NOEXCEPT_TYPE_SPEC;

using decode_f = ptrdiff_t (*)(u8_sv_t src,
                               u32_s_t& dst,
                               size_t dst_offset);

#else

using decode_next_f = ptrdiff_t (*)(const u8_char_t* src,
                                    size_t count,
                                    u32_char_t& code_point) U8D_NOEXCEPT_TYPE_SPEC;

using decode_f = ptrdiff_t (*)(const u8_char_t* src,
                               size_t count,
                               u32_s_t& dst,
                               size_t dst_offset);

#endif
```

For the remainder of this section, we will focus on the C-like pointer API.

#### `decode_next`

`decode_next` will attempt to decode a single code point given an input sequence of UTF-8 encoded code units. The start of the input sequence, `src`, should be on a code point boundary. Up to `count` code units from `src` are read. On success, the number of code units read is returned; on failure or for invalid input sequences, `DECODE_NEXT_STATUS::ERROR` (`-1`) is returned. The decoded code point is placed in the output parameter `code_point`.

#### `decode`

`decode` will attempt to decode all code points given an input sequence of UTF-8 encoded code units. The start of the input sequence, `src`, should be on a code point boundary. Up to `count` code units from `src` are read. On success, the number of code points written is returned; on failure or for invalid input sequences, `DECODE_STATUS::ERROR` (`-1`) is returned. The decoded code points are placed in the output parameter `dst` starting at `dst_offset` (`0` by default). Note that this does not verify that the size of `dst` is sufficient for the output, so be sure to allocate enough space in `dst`. This can be ensured for an arbitrary UTF-8 input sequence by allocating as many code points as there are code units because there is at most one code point per code unit (pure ASCII).

### Tests

There are three basic sets of tests for both `decode_next` and `decode`:

1. handling of empty, size zero string input
2. verification of correct detection of invalid UTF-8 input sequences
3. round-trip decoding of every possible valid UTF-8 code unit sequence

Test 2 uses an input vector of invalid UTF-8 sequences, while the round-trip test uses `iconv(3)` to convert UTF-32 code points up to the end of the Supplementary Private Use Area B (U+10FFFF) to UTF-8 encoded code units and then verifies that the original code point is retrieved by the decoder implementation.

Tests can be invoked directly at `./tests/tests` or using `ctest`.

### Benchmarks

The benchmarks decode the UTF-8 encoded data files located in `benchmarks/libutf8decoders-bench-data/data`, which should have be retrieved using `git submodule update --init` during installation. There are two sets of data: stress tests and Wikipedia data. The former dataset is from Bob Steagall's KEWB decoder repository and consists of three files with varying levels of ASCII data:

 - `0`: 100% ASCII
 - `1`: 0% ASCII and 100% CJK characters
 - `2`: 50% percent space (ASCII) and 50% CJK characters

 The latter dataset consists of Wikipedia articles for various human languages' pages in their respective languages. This currently consists of:

 - `English_language-en`
 - `Hindi_language-hi`
 - `Japanese_language-ja`
 - `Korean_language-ko`
 - `Portuguese_language-pt`
 - `Russian_language-ru`
 - `Swedish_language-sv`
 - `Chinese_language-zh`

Benchmarks can either be invoked directly using `./benchmarks/benchmarks` or by using `scripts/run-benchmarks.sh`. The benchmarks use Google benchmark, and the benchmark script is configured to automatically build the benchmarks for a given compiler and CMake configuration, run the tests, and output them in JSON format. These output JSON benchmark results can be analyzed and plotted using `scripts/analyze-benchmarks.py` (requires `matplotlib` and `numpy`). A convenience script `scripts/benchmark-analyze.sh` also exists which both runs and analyzes the benchmarks.

See `libutf8decoders-bench-results`[17] for some benchmark results.

## License

The contents of this repository are licensed under the MIT license, except where otherwise noted.

## References

1. [Wikipedia - UTF-8](https://en.wikipedia.org/wiki/UTF-8)
2. [Bjoern Hoehrmann UTF-8](http://bjoern.hoehrmann.de/utf-8/decoder/dfa/)
3. [Boost.Text reference](https://tzlaine.github.io/text/doc/html/index.html)
4. [Boost.Text source repository](https://github.com/tzlaine/text)
5. [Branchless UTF-8 decoder](https://nullprogram.com/blog/2017/10/06/)
6. [`std::codecvt::in`](https://en.cppreference.com/w/cpp/locale/codecvt/in)
7. [`iconv(3)`](http://man7.org/linux/man-pages/man3/iconv.3.html)
8. [ICU strings documentation](http://userguide.icu-project.org/strings/utf-8)
9. [ICU 8-bit unicode C-API reference](http://www.icu-project.org/apiref/icu4c/utf8_8h.html)
10. [ICU unicode C-API reference](http://icu-project.org/apiref/icu4c/ustring_8h.html)
11. [Bob Steagall C++Now 2018 source repository](https://github.com/BobSteagall/CppNow2018)
12. [CppCon 2018: Bob Steagall "Fast Conversion From UTF-8 with C++, DFAs, and SSE Intrinsics"](https://www.youtube.com/watch?v=5FQ87-Ecb-A)
13. [LLVM ConvertUTF API reference](https://llvm.org/doxygen/ConvertUTF_8h.html)
14. [Python 3 C-API reference - Unicode Objects and Codecs](https://docs.python.org/3/c-api/unicode.html)
15. [utf8proc source repository](https://github.com/JuliaStrings/utf8proc)
16. [`libutf8decoders-bench-data`](https://gitlab.com/mkrupcale/libutf8decoders-bench-data)
17. [`libutf8decoders-bench-results`](https://gitlab.com/mkrupcale/libutf8decoders-bench-results)
