#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"
TOP_DIR="${SCRIPT_DIR}/.."

DEFAULT_CXX=g++
DEFAULT_CXX_STD=17
DEFAULT_WORKING_DIR="${TOP_DIR}/benchmarks"
DEFAULT_NUM_TRIALS=1

ALL_DECODER_CMAKE_ARGS=()
FASTEST_DECODER_CMAKE_ARGS=("-DU8D_WITH_AV=OFF"
                            "-DU8D_WITH_BH=OFF"
                            "-DU8D_WITH_BOOST_TEXT=OFF"
                            "-DU8D_WITH_BRANCHLESS=OFF"
                            "-DU8D_WITH_CODECVT=OFF"
                            "-DU8D_WITH_ICONV=OFF"
                            "-DU8D_WITH_LLVM=OFF"
                            "-DU8D_WITH_PYTHON=OFF"
                            "-DU8D_WITH_UTF8PROC=OFF")

function print_usage ()
{
    echo "Usage: "
    echo "${BASH_SOURCE} [--help] <options> <cmake args...>"
    echo ""
    echo "Options"
    echo "  -h,--help"
    echo "    Prints help"
    echo "  -c,--cxx <compiler>"
    echo "    C++ compiler to use when building library and benchmarks"
    echo "  -d,--working-dir <dir>"
    echo "    Working directory for building library and benchmarks"
    echo "  -n,--num-trials"
    echo "    Number of benchmark trials or repetitions to run"
    echo "  -bp,--benchmark-prefix"
    echo "    Benchmark output file prefix"
    echo "  -pp,--plot-prefix"
    echo "    Plot title and output file prefix"
    echo "  -o,--output-dir <dir>"
    echo "    Benchmark results output directory"
    echo "  -s,--cxx-std <c++-std>"
    echo "    C++ standard to use when compiling library and benchmarks"
    echo ""
    echo "cmake args"
    echo "  The first cmake arg should be one of:"
    echo "    --"
    echo "      Passes subsequent arguments on to CMake"
    echo "    all"
    echo "      Uses default CMake arguments to test all UTF-8 decoders"
    echo "    fastest"
    echo "      Uses default CMake arguments to test only the fastest UTF-8 decoders"
}

function info ()
{
    echo "$*" 1>&2
}

function error ()
{
    info "$*"
    exit 1
}

function check_return ()
{
    if [ "$?" -ne "0" ]; then
        exit 1
    fi
}

# run command
function run ()
{
    info "+ $@"
    "$@"
    check_return
}

# cpu_model
function cpu_model ()
{
    echo $(lscpu | \
           grep 'Model name:' | \
           cut -d ':' -f 2 | \
           sed -e 's/^[ \t]*//;s/[ \t]*$//' \
               -e 's/\((tm)\|(R)\|Intel\|AMD\|CPU\|processor\)//gI' \
               -e 's/\(E\(3\|5\|7\)\)\([0-9]\)/\1-\3/' \
               -e 's/ @.*//')
}

# compiler_version cxx
function compiler_version ()
{
    local cxx="$1"
    echo $("${cxx}" --version | \
           head -n 1 | \
           grep -oP '[0-9]+(\.[0-9]+)' | \
           head -n 1)
}

# setup working_dir
function setup ()
{
    local working_dir="$1"
    if [ ! -d "${working_dir}" ]; then
        run mkdir -p "${working_dir}"
    else
        info "'${working_dir}' already exists. Continuing."
    fi
}

# run_benchmarks working_dir output_dir num_trials benchmark_prefix cxx cxx_std <cmake_args...>
function run_benchmarks ()
{
    local working_dir="$1"; shift
    local output_dir="$1"; shift
    local num_trials="$1"; shift
    local benchmark_prefix="$1"; shift
    local cxx="$1"; shift
    local cxx_std="$1"; shift
    local cmake_args=("$@")
    run "${SCRIPT_DIR}"/run-benchmarks.sh  \
            -b "${working_dir}"/build      \
            -c "${cxx}"                    \
            -n "${num_trials}"             \
            -p "${benchmark_prefix}"       \
            -o "${output_dir}"             \
            -s "${cxx_std}"                \
            "${cmake_args[@]}" |&          \
        tee "${working_dir}"/run-benchmarks.log
}

# analyze_benchmarks output_dir plot_prefix benchmark_file
function analyze_benchmarks ()
{
    local output_dir="$1"
    local plot_prefix="$2"
    local benchmark_file="$3"
    run "${SCRIPT_DIR}"/analyze-benchmarks.py \
          -f "$(basename ${benchmark_file})"  \
          -fo                                 \
          -fd "${output_dir}"                 \
          -ff svg                             \
          -fbe svg                            \
          -ptitle "${plot_prefix}"            \
          -psort                              \
          -ptime cpu                          \
          -si m                               \
          "${output_dir}"
}

positional=()
while [ $# -gt 0 ]; do
    case "$1" in
        -h|--help)
            print_usage
            exit 0
            ;;
        -d|--working-dir)
            working_dir="$2"
            shift 2
            ;;
        -c|--cxx)
            cxx="$2"
            shift 2
            ;;
        -n|--num-trials)
            num_trials="$2"
            shift 2
            ;;
        -o|--output-dir)
            output_dir="$2"
            shift 2
            ;;
        -bp|--benchmark-prefix)
            benchmark_prefix="$2"
            shift 2
            ;;
        -pp|--plot-prefix)
            plot_prefix="$2"
            shift 2
            ;;
        -s|--cxx-std)
            cxx_std="$2"
            shift 2
            ;;
        *)
            positional+=("$1")
            shift
            ;;
    esac
done
set -- "${positional[@]}" # restore positional parameters

cxx="${cxx:-${DEFAULT_CXX}}"
cxx_std="${cxx_std:-${DEFAULT_CXX_STD}}"
working_dir="${working_dir:-${DEFAULT_WORKING_DIR}}"
num_trials="${num_trials:-${DEFAULT_NUM_TRIALS}}"

DEFAULT_OUTPUT_DIR="${working_dir}/results/${cxx}-$(compiler_version "${cxx}")-${cxx_std}"
DEFAULT_BENCHMARK_FILE_PREFIX="$(cpu_model | sed -e 's/ /-/g')_"
DEFAULT_PLOT_PREFIX="${cxx} $(compiler_version "${cxx}") C++${cxx_std} $(cpu_model) "
output_dir="${output_dir:-${DEFAULT_OUTPUT_DIR}}"
benchmark_prefix="${benchmark_prefix:-${DEFAULT_BENCHMARK_FILE_PREFIX}}"
plot_prefix="${plot_prefix:-${DEFAULT_PLOT_PREFIX}}"

cmake_args=("$@")
case "${cmake_args[0]}" in
    --)
        cmake_args=("${cmake_args[@]:1}")
        ;;
    all)
        cmake_args=("${ALL_DECODER_CMAKE_ARGS[@]}")
        ;;
    fastest)
        cmake_args=("${FASTEST_DECODER_CMAKE_ARGS[@]}")
        ;;
    *)
        print_usage
        exit 0
        ;;
esac

setup "${working_dir}"
run_benchmarks "${working_dir}"      \
               "${output_dir}"       \
               "${num_trials}"       \
               "${benchmark_prefix}" \
               "${cxx}"              \
               "${cxx_std}"          \
               "${cmake_args[@]}"
benchmark_file="$(grep -oP '(?<=--benchmark_out=).*$' "${working_dir}"/run-benchmarks.log)"
rm "${working_dir}"/run-benchmarks.log
analyze_benchmarks "${output_dir}"     \
                   "${plot_prefix}"    \
                   "${benchmark_file}"
