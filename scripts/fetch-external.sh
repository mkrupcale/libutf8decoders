#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

BOB_STEAGALL_CPPCON2018_BASE_URL="https://github.com/BobSteagall/CppNow2018/raw/master/FastConversionFromUTF-8/code"
BOOST_TEXT_BASE_URL="https://github.com/tzlaine/text/raw/master/include/boost/text"
BRANCHLESS_BASE_URL="https://github.com/skeeto/branchless-utf8/raw/master"
LLVM_BASE_URL="https://github.com/llvm-mirror/llvm/raw/master"

function print_usage ()
{
    echo "Usage: "
    echo "${BASH_SOURCE} [--help] <decoder...>"
    echo ""
    echo "Options"
    echo "  -h,--help"
    echo "    Prints help"
    echo ""
    echo "Decoder"
    echo "  all"
    echo "    Fetch all external decoders"
    echo "  av"
    echo "    Fetch AV decoder"
    echo "  boost.text"
    echo "    Fetch Boost.Text decoder"
    echo "  branchless"
    echo "    Fetch branchless decoder"
    echo "  kewb"
    echo "    Fetch KEWB decoder"
    echo "  llvm"
    echo "    Fetch LLVM decoder"
}

# download url file
function download ()
{
    local url="$1"
    local file="$2"
    wget -O "${file}" "${url}"
}

# download_all base_url destdir files
function download_all ()
{
    local base_url="$1"; shift
    local destdir="$1"; shift
    for f in "$@"; do
        download "${base_url}/${f}" "${destdir}/${f}"
    done
}

# git_submodule_update path
function git_submodule_update ()
{
    local path="$1"
    git submodule update --init "${path}"
}

# apply_patch patch_file
function apply_patch ()
{
    local patch_file="$1"
    patch -p1 < "${patch_file}"
}

function fetch_av ()
{
    local base_url="${BOB_STEAGALL_CPPCON2018_BASE_URL}/test"
    local destdir="external/av"
    local files=("av_utf8.c" "av_utf8.h")
    download_all "${base_url}" "${destdir}" "${files[@]}"
    apply_patch "${destdir}/error-return-negative-utf8_to_wchar.patch"
}

function fetch_boost_text ()
{
    local base_url="${BOOST_TEXT_BASE_URL}"
    local destdir="external/boost/text"
    local files=("config.hpp" "utf8.hpp")
    download_all "${base_url}" "${destdir}" "${files[@]}"
}

function fetch_branchless ()
{
    local destdir="external/branchless-utf8"
    download "${BRANCHLESS_BASE_URL}/utf8.h" "${destdir}/utf8.h"
    apply_patch "${destdir}/branchless-utf8-utf8.h-signature.patch"
}

function fetch_kewb ()
{
    local base_url="${BOB_STEAGALL_CPPCON2018_BASE_URL}/src"
    local destdir="external/kewb"
    local files=("utf_utils.cpp" "utf_utils.h")
    download_all "${base_url}" "${destdir}" "${files[@]}"
    apply_patch "${destdir}/kewb-utf8-utf_utils.h-public-Advance.patch"
}

function fetch_llvm ()
{
    local destdir="external/llvm/Support"
    download "${LLVM_BASE_URL}/lib/Support/ConvertUTF.cpp" "${destdir}/ConvertUTF.cpp"
    download "${LLVM_BASE_URL}/include/llvm/Support/ConvertUTF.h" "${destdir}/ConvertUTF.h"
}

while [ $# -gt 0 ]; do
    case "$1" in
        -h|--help)
            print_usage
            exit 0
            ;;
        "all")
            FETCH_AV=true
            FETCH_BOOST_TEXT=true
            FETCH_BRANCHLESS=true
            FETCH_KEWB=true
            FETCH_LLVM=true
            break
            ;;
        "av")
            FETCH_AV=true
            ;;
        "boost.text")
            FETCH_BOOST_TEXT=true
            ;;
        "branchless")
            FETCH_BRANCHLESS=true
            ;;
        "kewb")
            FETCH_KEWB=true
            ;;
        "llvm")
            FETCH_LLVM=true
            ;;
    esac
    shift
done

if [ "${FETCH_AV}" = true ]; then
    fetch_av
fi

if [ "${FETCH_BOOST_TEXT}" = true ]; then
    fetch_boost_text
fi

if [ "${FETCH_BRANCHLESS}" = true ]; then
    fetch_branchless
fi

if [ "${FETCH_KEWB}" = true ]; then
    fetch_kewb
fi

if [ "${FETCH_LLVM}" = true ]; then
    fetch_llvm
fi
